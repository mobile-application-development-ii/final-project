package com.defalt.manifesto

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.defalt.manifesto.Adapter.CoinAdapter
import com.defalt.manifesto.Common.Common
import com.defalt.manifesto.Interface.OnLoad
import com.defalt.manifesto.Model.CoinModel
import com.defalt.manifesto.Model.ResModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException


class MainActivity : AppCompatActivity(),OnLoad {
    //Declare var
    private var items:MutableList<CoinModel> = ArrayList()
    private lateinit var adapter: CoinAdapter
    private lateinit var client: OkHttpClient
    private lateinit var request: Request


    override fun onLoadDetail(){
        if (items.size <= Common.MAX_COIN_LOAD){
            loadNext10Coin(items.size)
        }else{
            Toast.makeText(this@MainActivity,"Data max is "+Common.MAX_COIN_LOAD,Toast.LENGTH_SHORT).show()
        }
    }

    private fun loadNext10Coin(index: Int) {
        client = OkHttpClient()
        request = Request.Builder()
            .url(String.format("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?limit=10&convert=THB",index))
            .addHeader("X-CMC_PRO_API_KEY",Common.API_KEY)
            .build()

        swipe_to_refresh.isRefreshing=true
        client.newCall(request)
            .enqueue(object : Callback{
                override fun onFailure(call: Call, e: IOException) {
                    Log.d("ERROR",e.toString())
                }

                override fun onResponse(call: Call, response: Response) {
                    val body = response.body()!!.string()
                    Log.d("RESPOND",body)
                    val gson = Gson()
                    var newCoin:ResModel = gson.fromJson(body, object :TypeToken<ResModel>(){}.type)
                    val newItems = newCoin.data as MutableList<CoinModel>
                    runOnUiThread{
                        items.addAll(newItems)
                        adapter.setLoaded()
                        adapter.updateData(items)

                        swipe_to_refresh.isRefreshing = false
                    }
                }
            })
    }

    private fun loadFirst20Coin() {
        client = OkHttpClient()
        request = Request.Builder()
            .url(String.format("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?start=1&limit=20&convert=THB"))
            .addHeader("X-CMC_PRO_API_KEY",Common.API_KEY)
            .build()

        swipe_to_refresh.isRefreshing=true
        client.newCall(request)
            .enqueue(object : Callback{
                override fun onFailure(call: Call, e: IOException) {
                    Log.d("ERROR",e.toString())
                }

                override fun onResponse(call: Call, response: Response) {
                    val body = response.body()!!.string()
                    Log.d("RESPOND",body)
                    val gson = Gson()
                    var coin:ResModel = gson.fromJson(body, object :TypeToken<ResModel>(){}.type)
                    Log.d("CAST",coin.toString())
                    items = coin.data as MutableList<CoinModel>
                    var status = coin.status
                    Log.d("StatusRes",status.toString())
                    runOnUiThread{
                        adapter.updateData(items)
                        swipe_to_refresh.isRefreshing = false
                    }
                }
            })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            val intent = Intent(this,ConvertActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE,items.toString())
                Log.d("ITEMS",items.toString())
            }
            startActivity(intent)
        }

        swipe_to_refresh.post { loadFirst20Coin() }

        swipe_to_refresh.setOnRefreshListener {
            items.clear() // clear all items
            loadFirst20Coin()
            setUpAdapter()
        }
        coin_recycler_view.layoutManager = LinearLayoutManager(this)
        setUpAdapter()
    }

    private fun setUpAdapter() {
        adapter = CoinAdapter(coin_recycler_view,this,items)
        coin_recycler_view.adapter =adapter
        adapter.setLoadDetail(this)
    }
}
