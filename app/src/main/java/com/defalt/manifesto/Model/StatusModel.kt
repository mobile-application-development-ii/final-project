package com.defalt.manifesto.Model

class StatusModel {
    var timeStamp:String? = null
    var errorCode:String? = null
    var errorMessage:String? = null
    var elapsed:String? = null
    var creditCount:String? = null
    var notice:String? = null
    var totalCount:String? = null

    constructor()
    constructor(
        timeStamp: String?,
        errorCode: String?,
        errorMessage: String?,
        elapsed: String?,
        creditCount: String?,
        notice: String?,
        totalCount: String?
    ) {
        this.timeStamp = timeStamp
        this.errorCode = errorCode
        this.errorMessage = errorMessage
        this.elapsed = elapsed
        this.creditCount = creditCount
        this.notice = notice
        this.totalCount = totalCount
    }

    override fun toString(): String {
        return "StatusModel(timeStamp=$timeStamp, errorCode=$errorCode, errorMessage=$errorMessage, elapsed=$elapsed, creditCount=$creditCount, notice=$notice, totalCount=$totalCount)"
    }

}