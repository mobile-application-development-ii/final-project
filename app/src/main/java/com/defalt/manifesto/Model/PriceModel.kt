package com.defalt.manifesto.Model

import android.util.Log

class PriceModel {
    var price:String? =null
    var percent_change_1h:String?=null
    var percent_change_24h:String?=null
    var percent_change_7d:String?=null

    constructor()
    constructor(
        price: String?,
        percent_change_1h: String?,
        percent_change_24h: String?,
        percent_change_7d: String?
    ) {
        this.price = price
        this.percent_change_1h = percent_change_1h
        this.percent_change_24h = percent_change_24h
        this.percent_change_7d = percent_change_7d
        Log.d("PriceModel",toString())
    }

    override fun toString(): String {
        return "PriceModel(price=$price, percent_change_1h=$percent_change_1h, percent_change_24h=$percent_change_24h, percent_change_7d=$percent_change_7d)"
    }

}