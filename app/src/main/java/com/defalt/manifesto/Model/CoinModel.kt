package com.defalt.manifesto.Model

class CoinModel {
    var id:String?=null
    var name:String?=null
    var symbol:String?=null
    var quote:QuoteModel?=null
    constructor()
    constructor(id: String?, name: String?, symbol: String?, quote: QuoteModel?) {
        this.id = id
        this.name = name
        this.symbol = symbol
        this.quote = quote
    }

    override fun toString(): String {
        return "CoinModel(id=$id, name=$name, symbol=$symbol, quote=$quote)"
    }

}