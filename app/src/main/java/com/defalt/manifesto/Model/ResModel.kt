package com.defalt.manifesto.Model

class ResModel {
    var status: StatusModel? =null
    var data: List<CoinModel>?=null

    constructor()
    constructor(status: StatusModel, data: List<CoinModel>) {
        this.status = status
        this.data = data
    }

    override fun toString(): String {
        return "ResModel(status=$status, data=$data)"
    }

}