package com.defalt.manifesto.Model

class QuoteModel {
    var THB:PriceModel?=null

    constructor()
    constructor(THB: PriceModel?) {
        this.THB = THB
    }

    override fun toString(): String {
        return "QuoteModel(THB=$THB)"
    }

}