package com.defalt.manifesto.Adapter

import android.app.Activity
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.defalt.manifesto.Common.Common
import com.defalt.manifesto.Interface.OnLoad
import com.defalt.manifesto.MainActivity
import com.defalt.manifesto.Model.CoinModel
import com.defalt.manifesto.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.coin_layout.view.*
import java.lang.StringBuilder
import kotlin.math.roundToInt

class CoinViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
    var coinIcon = itemView.coinIcon
    var coinSymbol = itemView.coinSymbol
    var coinName = itemView.coinName
    var coinPrice = itemView.priceThb
    var oneHourChange = itemView.oneHour
    var twentyHourChange = itemView.twentyFourHour
    var sevenDayChange = itemView.sevenDay

}

class CoinAdapter(recyclerView: RecyclerView,internal var activity: Activity,var items:List<CoinModel>):
    RecyclerView.Adapter<CoinViewHolder>() {

    var loadMore:OnLoad?=null
    var isLoading:Boolean=false
    var visibleThresholds=5
    var lastVisibleItem:Int=0
    var totalItemCount:Int=0

    init {
        var linearLayout = recyclerView.layoutManager as LinearLayoutManager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = linearLayout.findLastVisibleItemPosition()
                if(!isLoading && totalItemCount <= lastVisibleItem+visibleThresholds){
                    if(loadMore != null)
                        loadMore!!.onLoadDetail()
                    isLoading=true

                }
            }
        })
    }

    fun setLoadDetail(loadMore:OnLoad){
        this.loadMore = loadMore
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinViewHolder {
        val view = LayoutInflater.from(activity)
            .inflate(R.layout.coin_layout,parent,false)
        return CoinViewHolder(view)
    }

    override fun onBindViewHolder(holder: CoinViewHolder, position: Int) {
        val coinModel = items[position]

        val  item = holder as CoinViewHolder

        item.coinName.text = coinModel.name
        item.coinSymbol.text = coinModel.symbol
        var price = (coinModel.quote!!.THB!!.price!!.toDouble() * 100.0).roundToInt() / 100.0
        Log.d("PRICE",price.toString())
        item.coinPrice.text = price.toString()
        var one = (coinModel.quote!!.THB!!.percent_change_1h!!.toDouble() * 100.0).roundToInt() / 100.0
        item.oneHourChange.text = one.toString() +"%"
        var twenty =(coinModel.quote!!.THB!!.percent_change_24h!!.toDouble() * 100.0).roundToInt() / 100.0
        item.twentyHourChange.text = twenty.toString()+"%"
        var seven = (coinModel.quote!!.THB!!.percent_change_7d!!.toDouble() * 100.0).roundToInt() / 100.0
        item.sevenDayChange.text = seven.toString()+"%"

        Picasso.with(activity.baseContext)
            .load(StringBuilder(Common.imageUrl)
                .append(coinModel.symbol!!.lowercase())
                .append(".png")
                .toString())
            .into(item.coinIcon)
        //set color 1h
        item.oneHourChange.setTextColor(if(coinModel.quote!!.THB!!.percent_change_1h!!.contains("-"))
            Color.parseColor("#FE0000")
        else
            Color.parseColor("#32CD32"))
        //set color 24h
        item.twentyHourChange.setTextColor(if(coinModel.quote!!.THB!!.percent_change_24h!!.contains("-"))
            Color.parseColor("#FE0000")
        else
            Color.parseColor("#32CD32"))
        //set color 7d
        item.sevenDayChange.setTextColor(if(coinModel.quote!!.THB!!.percent_change_7d!!.contains("-"))
            Color.parseColor("#FE0000")
        else
            Color.parseColor("#32CD32"))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setLoaded(){
        isLoading = false
    }
    fun updateData(coinModels: List<CoinModel>){
        this.items = coinModels
        notifyDataSetChanged()
    }
}
